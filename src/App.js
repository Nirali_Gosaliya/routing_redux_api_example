
import './App.css';
import NavBar from './Components/NavBar'
import Home from './Components/Home'
import About from './Components/About'
import {Routes,Route} from 'react-router-dom'

import Features from './Components/Features';

function App() {
  return (
    <div className="App">
    <NavBar />
    <Routes>
      <Route path="/" element={<Home />}></Route>
      <Route path="about" element={<About />}></Route>
      <Route path="/features" element={<Features />}></Route>
    </Routes>

    </div>
  );
}

export default App;
