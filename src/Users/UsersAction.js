import { FETCH_USERS_FAILURE, FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS } from "./UsersTypes"
import axios from 'axios'


 const fetchUsers = () => {
     return{
    type: FETCH_USERS_REQUEST
     }
}

 const fetchUsersSuccess = (users) => {
     return{
    type: FETCH_USERS_SUCCESS,
    payload: users
     }
}

 const fetchUsersFailure = (error) => {
     return{
    type: FETCH_USERS_FAILURE,
    payload: error
     }
}

export const fetchData = () => {
    return (dispatch) =>{
        axios.get("https://jsonplaceholder.typicode.com/todos")
        .then(response =>{
            dispatch(fetchUsersSuccess(response.data))
        })
        .catch(error =>{
            dispatch(fetchUsersFailure(error.message))
        })
    }
}

