import React,{useEffect} from 'react'
import { fetchData } from './UsersAction'
import {connect} from 'react-redux'

function UserContainer({userDatafetches,fetchData}){
    useEffect(()=>{
        fetchData();
    },[])
  return (
    <div>
        {userDatafetches.loading ? (
          <h2>Loading....</h2>
        ): userDatafetches.error ? (
          <h2>{userDatafetches.error}</h2>
        ):(
          <div>
              <table border="2">
                <thead>
                  <tr>
                    <td>User Id</td>
                    <td>Title</td>
                    <td>Id</td>
                  </tr>
                </thead>

                <tbody>
                    {userDatafetches.users.map(user => 
                      <tr>
                        <td>{user.userId}</td>
                        <td>{user.title}</td>
                        <td>{user.id}</td>
                      </tr>
                      )}
                </tbody>

              </table>
            {/* {userDatafetches && userDatafetches.users && userDatafetches.users.map(user => <p>{user.title}</p>)} */}
            </div>
        )}
 
    </div>
  )
}

const mapStateToProps = (state) => {
    return{
        userDatafetches: state.fetchUser
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () =>dispatch(fetchData())
  }
}



export default connect(
  mapStateToProps,mapDispatchToProps)(UserContainer);