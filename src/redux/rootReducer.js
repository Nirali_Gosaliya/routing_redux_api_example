import UsersReducer from "../Users/UsersReducer";
import { combineReducers } from "redux";


const rootReducer = combineReducers({
    fetchUser: UsersReducer
})

export default rootReducer;