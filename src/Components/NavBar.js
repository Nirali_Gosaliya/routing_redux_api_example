import React from 'react'
import '../Assets/CSS/NavBar.css'
import {NavLink} from 'react-router-dom'

function NavBar() {
  return (
    <div>
      <nav>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/features">Features</NavLink>
          <NavLink to="/products">Products</NavLink>
        </nav>
    </div>
  )
}

export default NavBar;