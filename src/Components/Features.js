import React from 'react'
import UserContainer from '../Users/UserContainer';
import {Provider} from 'react-redux'
import store from '../redux/store';

function Features() {
  return (
    <Provider store={store}>
        <UserContainer />
   </Provider>
  )
}

export default Features;